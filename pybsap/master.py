from pybsap.base import PyBSAPBase


class PyBSAPMaster(PyBSAPBase):

    poll_period = 20

    def __init__(self, **kwargs):
        super(PyBSAPMaster, self).__init__(**kwargs)

