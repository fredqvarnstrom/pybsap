import sys
import binascii
import crc16

if sys.version_info >= (3, 0):  # in case of python3
    is_p3 = True
else:
    is_p3 = False


def calc_crc_from_hex_string(hex_str):
    """
    Calculate CRC-CCITT from hex string
    :param hex_str:
    :return:
    """
    byte_seq = binascii.unhexlify(hex_str)
    crc = crc16.crc16xmodem(byte_seq, 0xffff)
    return '{:04X}'.format(crc & 0xffff)


def to_hex(s):
    """
    Convert ASCII string to HEX string
    """
    # If instance is not iterable, change it to the list
    if not hasattr(s, '__iter__'):
        s = [s, ]

    if len(s) == 0:
        return ''
    lst = []
    for ch in s:
        if is_p3:  # in case of python3
            if isinstance(ch, int):
                hv = "%x" % ch
            else:
                hv = hex(ord(ch)).replace('0x', '')
        else:  # python2
            hv = hex(ord(ch)).replace('0x', '')

        if len(hv) == 1:
            hv = '0' + hv
        lst.append(hv)

    return reduce(lambda x, y: x + y, lst)
