"""

    Message handler for BSAP protocol

"""

# ##################################################################################
#                                 Constant Message Values
# ##################################################################################
import binascii

from pybsap.utils.common import calc_crc_from_hex_string

DLE = '10'
SOH = '01'
STX = '02'
ETX = '03'

FUNCTION_CODE = {
    'POLL': '85',
    'DOWN-ACK': '86',
    'ACK': '86',        # Same with `DOWN-ACK`
    'ACK-NODATA': '87',
    'NAK': '95',
    'UTA': '8B',
    'FC': '81',
}


# ##################################################################################
#                                 Master => Slave
# ##################################################################################

def poll_msg(address=00, sn=00, pri=0):
    """
    Poll message is used by a master to interrogate its slaves and determine if the
    slave is alive, and if so, to solicit data.
    Message Format:
        DLE,STX,ADDR,SER,POLL,PRI,DLE,ETX,CRC
    :param address:     Address of polled node
    :param sn:          Serial number of message
    :param pri:         Priority of requested data
        A PRI of 0 indicates that alarms or data can be accepted; A PRI of 10H
        indicates that alarms cannot be accepted.
    :return:
    """
    try:
        msg = DLE + STX + '{:02X}{:02X}'.format(address, sn) + FUNCTION_CODE['POLL'] + \
              '{:02X}'.format(pri) + DLE + ETX
        msg += calc_crc_from_hex_string(msg)
        return binascii.unhexlify(msg)
    except Exception as e:
        print 'Failed to compose POLL message: {}'.format(e)


def up_ack_msg(slv_addr=00, sn=00, ack_sn=00):
    """
    The UP-ACK message is used by the master to inform the slave that it successfully
    received and buffered the message.
    Message Format:
        DLE,STX,ADDR,SERM,UTA,SERS,DLE,ETX,CRC
    :param slv_addr:    Local address of slave
    :param sn:          Master's message serial number
    :param ack_sn:      Serial number of message ACK'd
    :return:
    """
    try:
        msg = DLE + STX + '{:02X}{:02X}'.format(slv_addr, sn) + FUNCTION_CODE['UTA'] + \
            '{:02X}'.format(ack_sn) + DLE + ETX
        msg += calc_crc_from_hex_string(msg)
        return binascii.unhexlify(msg)
    except Exception as e:
        print 'Failed to compose UP-ACK message: {}'.format(e)


# ##################################################################################
#                                  Slave => Master
# ##################################################################################

def ack_msg(sn=00, slv_addr=00, node_status=00, buffers_cnt=00, ack_type='DOWN-ACK'):
    """
    :param ack_type: `ACK`(or `DOWN-ACK`), `ACK-NODATA`, `NAK`
        - The ACK (also called DOWN-ACK):
            This is used by a slave to acknowledge receipt of a data message (not a POLL)
            from its master.
        - ACK-NODATA:
            This is used by a slave to acknowledge receipt of a POLL and indicate that it
            has no data messages to respond with.
        - NAK:
            This is used by a slave to indicate that a message other than a POLL was
            received but there is insufficient buffer space available.
        Message Format:
            DLE,STX,ADDR,SER,FUNCTION_CODE,SLV,NSB,DOWN,DLE,ETX,CRC

    :param sn:              Serial number of message ACK'd
    :param slv_addr:        Local address of slave responding
    :param node_status:     Node Status Byte
    :param buffers_cnt:     Number of buffers in use
    :return:
    """
    try:
        msg = DLE + STX + '00{:02X}'.format(sn) + FUNCTION_CODE[ack_type] + \
            '{:02X}{:02X}{:02X}'.format(slv_addr, node_status, buffers_cnt) + DLE + ETX
        msg += calc_crc_from_hex_string(msg)
        return binascii.unhexlify(msg)
    except Exception as e:
        print 'Failed to compose ACK message: {}'.format(e)


# ##################################################################################
#                                  DIAL_UP_ACK message
# ##################################################################################

"""
    When the Slave Port successfully initiates a dial-up session, it monitors the line
    for receipt of any Poll message to any node.
    The poll message could be a standard Poll or an Expanded BSAP Poll, and can target
    any node. The DIAL_UP_ACK is sent in response to any poll.
"""


def standard_dial_up_ack_msg(node_address=00, sn=00, slv_group_num=00, slv_addr=00,
                             port_type=00, nrt_flag=1, nrt_ver=0):
    """
    Message Format:
        DLE STX NA SN FC DSG DSA PT VNRT NVER DLE ETX CRC1 CRC2
    :param node_address:    Node Address 0 (response to master)
    :param sn:              Serial number from POLL
    :param slv_group_num:   Dial-up slave group number(0)
    :param slv_addr:        Dial-up slave address (1-127)
    :param port_type:       Port type (slave)
    :param nrt_flag:        Valid NRT Flag (1=valid; 0=invalid)
    :param nrt_ver:         NRT Version (0 if VNRT=0)
    :return:
    """
    try:
        # TODO: Clarify CRC1 & CRC2 value
        crc1 = '0000'
        crc2 = '0000'
        msg = DLE + STX + \
            '{:02X}{:02X}'.format(node_address, sn) + \
            FUNCTION_CODE['FC'] + \
            '{:02X}{:02X}{:02X}{:02X}{:02X}'.\
            format(slv_group_num, slv_addr, port_type, nrt_flag, nrt_ver) + \
            DLE + ETX + crc1 + crc2
        return binascii.unhexlify(msg)
    except Exception as e:
        print 'Failed to compose Standard DIAL_UP_ACK message: {}'.format(e)


def expanded_dial_up_ack_msg(group_num=00, node_addr=00, sn=00, slv_group_num=00,
                             slv_addr=00, port_type=00, nrt_flag=1, nrt_ver=0):
    """
    Message Format:
        DLE STX NA SN FC DSG DSA PT VNRT NVER DLE ETX CRC1 CRC2
    :param group_num:       Group number from Exp. Poll
    :param node_addr:       Node Address 0 (response to master)
    :param sn:              Serial number from POLL
    :param slv_group_num:   Dial-up slave group number
    :param slv_addr:        Dial-up slave address (1-127)
    :param port_type:       Port type (slave)
    :param nrt_flag:        Valid NRT Flag (1=valid; 0=invalid)
    :param nrt_ver:         NRT Version (0 if VNRT=0)
    :return:
    """
    try:
        # TODO: Clarify CRC1 & CRC2 value
        crc1 = '0000'
        crc2 = '0000'
        msg = DLE + SOH + \
            '{:02X}{:02X}{:02X}'.format(group_num, node_addr, sn) + \
            FUNCTION_CODE['FC'] + \
            '{:02X}{:02X}{:02X}{:02X}{:02X}'.format(
                slv_group_num, slv_addr, port_type, nrt_flag, nrt_ver) + \
            DLE + ETX + crc1 + crc2
        return binascii.unhexlify(msg)
    except Exception as e:
        print 'Failed to compose Expanded DIAL_UP_ACK message: {}'.format(e)


# ##################################################################################
#                                  Data Message
# ##################################################################################


def local_data_msg(local_addr=00, sn=00, dest_func_code=00, seq_num=00,
                   src_func_code=00, node_status=00, data=''):
    """
        Local messages are those which do not have to pass through any nodes to
        reach their destination. By definition, the first node to receive a local
        message is the destination.
            Message Format:
                DLE,STX,LADD,SER,DFUN,SEQ,SFUN,NSB,data,DLE,ETX,CRC
    :param local_addr:          Local address
    :param sn:                  Message serial number
    :param dest_func_code:      Destination function code
    :param seq_num:             Application sequence number
    :param src_func_code:       Source function code
    :param node_status:         Node Status Byte
    :param data:                Application-dependent data (up to 246 bytes)
    :return:
    """
    try:
        msg = DLE + STX + '{:02X}{:02X}{:02X}{:02X}{:02X}{:02X}'.\
            format(local_addr, sn, dest_func_code, seq_num, src_func_code, node_status) \
            + data + DLE + ETX
        msg += calc_crc_from_hex_string(msg)
        return binascii.unhexlify(msg)
    except Exception as e:
        print 'Failed to compose Local Data message: {}'.format(e)


def global_data_msg(local_addr=00, sn=00, dest_addr=00, src_addr=00,
                    ctl_byte=00, dest_func_code=00, seq_num=00,
                    src_func_code=00, node_status=00, data=''):
    """
    Global data messages are those which must pass through at least one master before
    reaching their destination.
    Message Format:
        DLE,STX,LADD,SER,DADD,SADD,CTL,DFUN,SEQ,SFUN,NSB,data,DLE,ETX,CRC
    :param local_addr:          Local address + 80H
    :param sn:                  Message serial number
    :param dest_addr:           Destination global address
    :param src_addr:            Source global address
    :param ctl_byte:            Control byte
    :param dest_func_code:      Destination function code
    :param seq_num:             Application sequence number
    :param src_func_code:       Source function code
    :param node_status:         Node Status Byte
    :param data:                Application-dependent data (up to 241 bytes)
    :return:
    """
    try:
        msg = DLE + STX + '{:02X}{:02X}{:02X}{:02X}{:02X}{:02X}{:02X}{:02X}{:02X}'.\
            format(local_addr, sn, dest_addr, src_addr, ctl_byte, dest_func_code,
                   seq_num, src_func_code, node_status) + data + DLE + ETX
        msg += calc_crc_from_hex_string(msg)
        return binascii.unhexlify(msg)
    except Exception as e:
        print 'Failed to compose Global Data Message: {}'.format(e)


# ##################################################################################
#                                  TS/NRT Message
# ##################################################################################

def ts_nrt_msg():
    """
    Local header:
        LADD = Local address
        SER = Message serial number
        DFUN = Destination function code
        SEQ = Application sequence number
        SFUN = Source function code
        NSB = Node Status Byte
    :return:
    """


if __name__ == '__main__':
    print poll_msg(address=03, sn=34, pri=16)
